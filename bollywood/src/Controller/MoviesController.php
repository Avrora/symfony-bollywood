<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Movies;
use App\Form\MoviesType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;



class MoviesController extends AbstractController {

    /**
     * @Route("/movies", name="movies")
     */
    public function first() {
       
        return $this->render("movies.html.twig", []);
    }

    /**
     * @Route("/movieForm", name="movies_form")
     */
    public function movieForm(Request $request) {
        $movies = new Movies;
        $form = $this->createForm(MoviesType::class, $movies);
        $form->add('submit', SubmitType::class, [
            'label' => 'Submit',
            'attr' => ['class' => 'btn btn-default pull-right'],
        ]);

        $form->handleRequest($request);
        dump($form);
        $moviesData = $form -> getData();
        return $this->render("movies.html.twig", [
            "form" => $form -> createView(),
            "moviesData" => $moviesData
        ]);
    }
}